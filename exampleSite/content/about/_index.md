---
title: About
weight: 1
---

This theme is designed by MakeJames.

## Colour Schemes



### Current

{{< rawhtml >}}
      <div class="row-colors" >
        <div class="color" style="background: var(--dark-accent)"></div>
        <div class="color" style="background: var(--light-shades)"></div>
        <div class="color" style="background: var(--main-color)"></div>
        <div class="color" style="background: var(--light-accent)"></div>
        <div class="color" style="background: var(--dark-shades)"></div>
      </div>
{{< /rawhtml >}}

{{< rawhtml >}}
    <details>
{{< /rawhtml >}}

### Business

`business`

{{< rawhtml >}}
      <div class="row-colors" >
        <div class="color" style="background: #74746D"></div>
        <div class="color" style="background: #B4A9A7"></div>
        <div class="color" style="background: #C5283D"></div>
        <div class="color" style="background: #E1D4CC"></div>
        <div class="color" style="background: #4A4A45"></div>
      </div>
{{< /rawhtml >}}

### Colorful

`colorful`

{{< rawhtml >}}
      <div class="row-colors" >
        <div class="color" style="background: #F1481E"></div>
        <div class="color" style="background: #36C3C3"></div>
        <div class="color" style="background: #F4BB1B"></div>
        <div class="color" style="background: #F1F1C6"></div>
        <div class="color" style="background: #2F6B8C"></div>
      </div>
{{< /rawhtml >}}

### Default

`default` or `living-room`

{{< rawhtml >}}
    <div class="row-colors" >
      <div class="color" style="background: #656EA4"></div>
      <div class="color" style="background: #CF8626"></div>
      <div class="color" style="background: #1A8985"></div>
      <div class="color" style="background: #ECE7C1"></div>
      <div class="color" style="background: #E9451B"></div>
    </div>
{{< /rawhtml >}}

### Wes-Anderson

`wes-anderson`

{{< rawhtml >}}
    <div class="row-colors" >
      <div class="color" style="background: #F2997A"></div>
      <div class="color" style="background: #BEC7AA"></div>
      <div class="color" style="background: #33848D"></div>
      <div class="color" style="background: #ECCD98"></div>
      <div class="color" style="background: #08597A"></div>
    </div>
{{< /rawhtml >}}
{{< rawhtml >}}
    </details>
{{< /rawhtml >}}


## Shapes

Shapes can be configured in the front matter of the page

```
Shape: square
Transform: 40
Color: --main-color
```

The `Shape` parameter takes the options of `square` and `triange`. The `Transform` parameter takes a numeric value that will be used to apply a rotational transformation to the generated shape.

The colour of the shape can also be defined, this will use one of the following values

- `--main-color`
- `--dark-shades`
- `--light-shades`
- `--dark-accent`

If not specified in the front-matter of the page the page will default to render a square with a random rotational value and random value from the above list.

### Squares

To see some preset square options please view the following pages

{{< section-list "about/shapes/square" >}}

### Triangles

To see some preset square options please view the following pages

{{< section-list "about/shapes/triangle" >}}
