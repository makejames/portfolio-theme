---
title: Shortcodes
weight: 5
---


## Rawhtml

Use `rawhtml` to insert html directly into to the rendered page, bypassing the normal content generation. Should be used sparingly.

## Section-list

Lists the pages at a given section. Defaults to the current section, but can take a parameter of a relative page.

Outputs a simple list.

