# Portfolio Theme

A simple and attractive theme with a focus on site responsiveness and accessibility.

## Roadmap

### v1.0

- [x] Heading Shapes
- [x] Pre-defined colour schemes
- [x] Default fonts
- [ ] Page heading image
- [ ] Page footer with site map
- [ ] Page Search
- [ ] Section Page Carousel
- [ ] Section Page Gallery
- [ ] Embed Images in content
- [ ] Embed Videos in content
- [ ] Contact form
- [ ] Social Media links
- [ ] Improved Markdown generation
- [ ] Increase page padding
- [ ] Fix menu brand display for mobile screens
- [ ] Icon fonts
- [ ] Refactored DRY CSS / SCSS
- [ ] Consistent CSS class and variable names

### Future Features

- [ ] Event planning integrations
    - [ ] Eventbrite
    - [ ] Pretix
- [ ] Embed [Open Street Maps](https://www.openstreetmap.org/)
- [ ] Nested menus
- [ ] Kofi integration
- [ ] Blog

### Technical Enhancements

- [ ] Declare Brand logo (replacing generated SVG)
- [ ] Enable User / Custom Fonts
- [ ] Animated mobile hamburger transition
- [ ] Page tags and Categories
- [ ] Improved Colour Schemes
- [ ] Improve OpenGraph Declarations

## Usage

You can use this theme in one of two ways, either as a go module or a git sub-module

```
hugo mod init gitlab.com/makejames/portfolio-theme
```

Or as a git sub-module

```
git submodule add https://gitlab.com/makejames/portfolio-theme
```

## Development

This project uses simple versioning of MAJOR.MINOR

Releases before `v1.0` should be considered unstable and subject to breaking changes between minor releases.

### Contributing

Development of this repository is done on branches. Branches are prefixed with a stub to mark the intention of the changes such as:

```
init/     # Structural or supporting changes to the repository itself
feature/  # A scoped feature or function
fix/      # Scoped to resolve a specific issue
docs/     # Documentation for the repository
content/  # Content for the example site
```

### Commit messages

Commit messages should be prefixed with the intent of the commit with a commit message written in the imperative.
