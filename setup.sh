#!/bin/sh
EXAMPLE_SITE="exampleSite/"
THEME_DIR="exampleSite/themes/portfolio-theme/"

mkdir -p $THEME_DIR
cp -r `ls -A | grep -v "exampleSite\|.git"` $THEME_DIR

cd $EXAMPLE_SITE
hugo serve --gc --disableFastRender --buildDrafts --noHTTPCache -c $(CONTENTDIR) --themesDir $(THEMEDIR)
