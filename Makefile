.POSIX:
SHELL=/bin/bash

DESTDIR=public/
EXAMPLE=exampleSite/
CONTENTDIR=$(EXAMPLE)content/
THEMEDIR=$(EXAMPLE)themes/

.PHONY: all
all: clean get build

.PHONY: clean
clean:
	@echo "Cleaning old build"
	rm -rf $(DESTDIR)
	rm -rf resources/ $(EXAMPLE)resources/
	rm -rf $(THEMEDIR)

.PHONY: get
get:
	@echo "Checking Hugo is installed"
	@type hugo

.PHONY: build
build:
	@echo "Building site"
	./build.sh

.PHONY: serve
serve: get
	@echo "Building site"
	./setup.sh
